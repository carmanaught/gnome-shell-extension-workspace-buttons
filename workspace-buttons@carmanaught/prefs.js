const { Gdk, Gio, GLib, GObject, Gtk, Pango } = imports.gi;
const Gettext   = imports.gettext;

const _ = Gettext.domain('workspace-buttons').gettext;
const _N = function(x) { return x; }

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

let provider = new Gtk.CssProvider();

provider.load_from_path(Me.dir.get_path() + '/prefs.css');
Gtk.StyleContext.add_provider_for_display(
    Gdk.Display.get_default(),
    provider,
    Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

const KEYS = Me.imports.keys;

const POSITIONS = [
    'left',
    'center',
    'right'
];
const BUTTONS = [
    'Primary',
    'Secondary'
];

function debug(val) {
    val = `[ Workspace Buttons ]--------> ${val}`;
    log(val);
}

const WorkspaceButtonsPrefsWidget = GObject.registerClass({
    GTypeName: 'WorkspaceButtonsPrefsWidget',
    Template: Me.dir.get_child('prefs.ui').get_uri(),
    InternalChildren: ['cmbPosition','swPositionIndexEnable','adjPositionIndex','rowPositionIndex',
        'spnPosition','swWrapAround','swClickActivate','rowButtonActivate','cmbButtonActivate',
        'swEmptyWorkspaceHide','rowEmptyWorkspace','swEmptyWorkspace','rowUrgentWorkspace',
        'swUrgentWorkspace','rowWkspNumber','swWkspNumber','rowWkspName','swWkspName','rowLabelSeparator',
        'txtSeparator','lblActivityInd','swActInd','boxActivityInd','txtEmptyInd','txtInactiveInd',
        'txtActiveInd','btnColorDefaults','btnUrgentColor','btnHoverColor','btnActiveColor',
        'btnInactiveColor','btnEmptyColor','lblPositionTitle', 'lblGeneralTitle', 'lblClickSmall',
        'lblLabelFormatTitle', 'lblEmptyHideSmall', 'lblSeparatorSmall', 'lblActivityTitle',
        'lblActivitySmall', 'lblLabelColorTitle'],
}, class WorkspaceButtonsPrefsWidget extends Gtk.Box {

    _init(params = {}) {
        super._init(params);
        this._settings = ExtensionUtils.getSettings();

        this.connect('realize', () => {

            // Set CSS against some labels here
            // Listbox title label CSS
            this._lblPositionTitle.css_classes = ['listbox-label'];
            this._lblGeneralTitle.css_classes = ['listbox-label'];
            this._lblLabelFormatTitle.css_classes = ['listbox-label'];
            this._lblActivityTitle.css_classes = ['listbox-label'];
            this._lblLabelColorTitle.css_classes = ['listbox-label'];

            // Small label CSS
            this._lblClickSmall.css_classes = ['small-label'];
            this._lblEmptyHideSmall.css_classes = ['small-label'];
            this._lblSeparatorSmall.css_classes = ['small-label'];
            this._lblActivitySmall.css_classes = ['small-label'];


            // Position Settings
            for (let i = 0; i < POSITIONS.length; i++) {
                this._cmbPosition.append_text(POSITIONS[i].charAt(0).toUpperCase() + POSITIONS[i].slice(1));
            }
            this._cmbPosition.set_active(POSITIONS.indexOf(this._settings.get_string(KEYS.buttonsPos)));
            this._cmbPosition.connect("changed", () => {
                this._settings.set_string(KEYS.buttonsPos, POSITIONS[this._cmbPosition.active]);
            });

            this._settings.bind(KEYS.buttonsPosChange, this._swPositionIndexEnable, "active", Gio.SettingsBindFlags.DEFAULT);
            this._swPositionIndexEnable.connect("notify::active", () => {
                 this._rowPositionIndex.set_sensitive(this._swPositionIndexEnable.active);
            });

            this._rowPositionIndex.set_sensitive(this._swPositionIndexEnable.active);

            this._spnPosition.set_value (this._settings.get_int(KEYS.buttonsPosIndex));
            this._spnPosition.connect("value-changed", () => {
                this._settings.set_int(KEYS.buttonsPosIndex, this._spnPosition.value);
            });


            // General Settings
            this._settings.bind(KEYS.wrapAroundMode, this._swWrapAround, "active", Gio.SettingsBindFlags.DEFAULT);

            this._settings.bind(KEYS.clickToActivate, this._swClickActivate, "active", Gio.SettingsBindFlags.DEFAULT);
            this._swClickActivate.connect("notify::active", () => {
                this._rowButtonActivate.set_sensitive(this._swClickActivate.active);
            });

            this._rowButtonActivate.set_sensitive(this._swClickActivate.active);

            for (let i = 0; i < BUTTONS.length; i++) {
                this._cmbButtonActivate.append_text(BUTTONS[i]);
            }
            this._cmbButtonActivate.set_active(BUTTONS.indexOf(this._settings.get_string(KEYS.buttonToActivate)));
            this._cmbButtonActivate.connect("changed", () => {
                this._settings.set_string(KEYS.buttonToActivate, BUTTONS[this._cmbButtonActivate.active]);
            });


            // Workspace Appearance/Label Format
            this._settings.bind(KEYS.emptyWorkHide, this._swEmptyWorkspaceHide, "active", Gio.SettingsBindFlags.DEFAULT);

            this._settings.bind(KEYS.emptyWorkStyle, this._swEmptyWorkspace, "active", Gio.SettingsBindFlags.DEFAULT);

            this._settings.bind(KEYS.urgentWorkStyle, this._swUrgentWorkspace, "active", Gio.SettingsBindFlags.DEFAULT);

            this._settings.bind(KEYS.numLabel, this._swWkspNumber, "active", Gio.SettingsBindFlags.DEFAULT);
            this._rowWkspNumber.set_sensitive(this._settings.get_boolean(KEYS.nameLabel) || this._settings.get_boolean(KEYS.indLabel) ? true : false)
            this._swWkspNumber.connect("notify::active", () => {
                // Disable workspace label separator if both workspace numbers and names are not enabled
                this._rowLabelSeparator.set_sensitive(this._swWkspNumber.active === true && this._swWkspName.active === true ? true : false)

                // Disable the ability to disable workspace names unless the activity indicators are
                // enabled as we have to have some sort of indicator
                if (this._swActInd.active === true) {
                    this._rowWkspName.set_sensitive(false);
                } else {
                    this._rowWkspName.set_sensitive(this._swWkspNumber.active === true || this._swActInd.active === true ? true : false);
                }
            });

            this._settings.bind(KEYS.nameLabel, this._swWkspName, "active", Gio.SettingsBindFlags.DEFAULT);
            this._rowWkspName.set_sensitive(this._settings.get_boolean(KEYS.indLabel) ? false : (this._settings.get_boolean(KEYS.numLabel) ? true : false));
            this._swWkspName.connect("notify::active", () => {
                this._setWkspName(this._swWkspName);

                // Disable workspace label separator if both workspace numbers and names are not enabled
                this._rowLabelSeparator.set_sensitive(this._swWkspNumber.active === true && this._swWkspName.active === true ? true : false)

                // Disable the ability to disable workspace numbers unless the activity indicators are
                // enabled as we have to have some sort of indicator
                this._rowWkspName.set_sensitive(this._swWkspNumber.active === true || this._swActInd.active === true ? true : false);
            });

            this._rowLabelSeparator.set_sensitive(this._swWkspNumber.active === true && this._swWkspName.active === true ? true : false)
            this._txtSeparator.set_text(this._settings.get_string(KEYS.labelSeparator));
            this._txtSeparator.connect("changed", () => { this._onSeparatorChanged() });
            this._txtSeparator.connect("activate", () => { this._onSeparatorChanged() });


            // Activity Indicators
            this._settings.bind(KEYS.indLabel, this._swActInd, "active", Gio.SettingsBindFlags.DEFAULT);
            this._swActInd.connect ("notify::active", () => {

                let actIndEnable = this._settings.get_boolean(KEYS.indLabel);
                let numIndEnable = this._settings.get_boolean(KEYS.numLabel);
                let nameIndEnable = this._settings.get_boolean(KEYS.nameLabel);

                if (actIndEnable === true) {
                    this._rowWkspNumber.set_sensitive(true);
                    this._rowWkspName.set_sensitive(false);
                } else {
                    if (nameIndEnable === false) {
                        this._rowWkspName.set_sensitive(true);
                        this._rowWkspNumber.set_sensitive(false);
                    } else if (numIndEnable === false) {
                        this._rowWkspName.set_sensitive(false);
                        this._rowWkspNumber.set_sensitive(true);
                    } else if (numIndEnable === true && nameIndEnable === true) {
                        this._rowWkspName.set_sensitive(true);
                        this._rowWkspNumber.set_sensitive(true);
                    }
                    if (numIndEnable === false && nameIndEnable === false) {
                        this._rowWkspName.set_sensitive(false);
                        this._swWkspName.active = true;
                        this._setWkspName(this._swWkspName);
                        this._rowWkspNumber.set_sensitive(true);
                    }
                }

                this._boxActivityInd.set_sensitive(actIndEnable);
                this._lblActivityInd.set_sensitive(actIndEnable);
            });

            let indList = this._settings.get_strv(KEYS.labelIndicators);

            this._txtEmptyInd.set_text(indList[0] !== undefined ? indList[0] : "");
            this._txtEmptyInd.connect ("changed", () => { this._onIndicatorChanged() });
            this._txtEmptyInd.connect ("activate", () => { this._onIndicatorChanged() });

            this._txtInactiveInd.set_text(indList[1] !== undefined ? indList[1] : "");
            this._txtInactiveInd.connect ("changed", () => { this._onIndicatorChanged() });
            this._txtInactiveInd.connect ("activate", () => { this._onIndicatorChanged() });

            this._txtActiveInd.set_text(indList[2] !== undefined ? indList[2] : "");
            this._txtActiveInd.connect ("changed", () => { this._onIndicatorChanged() });
            this._txtActiveInd.connect ("activate", () => { this._onIndicatorChanged() });


            // Workspace Label Color Settings
            this._colorCheckSet(this._btnUrgentColor, KEYS.urgentColor);
            this._btnUrgentColor.connect("color-set", (button) => {
                this._settings.set_string(KEYS.urgentColor, button.get_rgba().to_string());
            });

            this._colorCheckSet(this._btnHoverColor, KEYS.hoverColor);
            this._btnHoverColor.connect("color-set", (button) => {
                this._settings.set_string(KEYS.hoverColor, button.get_rgba().to_string());
            });

            this._colorCheckSet(this._btnActiveColor, KEYS.activeColor);
            this._btnActiveColor.connect("color-set", (button) => {
                this._settings.set_string(KEYS.activeColor, button.get_rgba().to_string());
            });

            this._colorCheckSet(this._btnInactiveColor, KEYS.inactiveColor);
            this._btnInactiveColor.connect("color-set", (button) => {
                this._settings.set_string(KEYS.inactiveColor, button.get_rgba().to_string());
            });

            this._colorCheckSet(this._btnEmptyColor, KEYS.emptyColor);
            this._btnEmptyColor.connect("color-set", (button) => {
                this._settings.set_string(KEYS.emptyColor, button.get_rgba().to_string());
            });

            this._btnColorDefaults.connect("clicked", () => {
                this._settings.reset(KEYS.urgentColor);
                this._colorCheckSet(this._btnUrgentColor, KEYS.urgentColor);
                this._settings.reset(KEYS.hoverColor);
                this._colorCheckSet(this._btnHoverColor, KEYS.hoverColor);
                this._settings.reset(KEYS.activeColor);
                this._colorCheckSet(this._btnActiveColor, KEYS.activeColor);
                this._settings.reset(KEYS.inactiveColor);
                this._colorCheckSet(this._btnInactiveColor, KEYS.inactiveColor);
                this._settings.reset(KEYS.emptyColor);
                this._colorCheckSet(this._btnEmptyColor, KEYS.emptyColor);
            })

        });
    }
    
    _setWkspName(object) {
        this._settings.set_boolean(KEYS.nameLabel, object.active);
    }

    _onSeparatorChanged() {
        this._settings.set_string(KEYS.labelSeparator, this._txtSeparator.get_text());
    }

    _onIndicatorChanged() {
        let arrIndicators = [];
        arrIndicators[0] = this._txtEmptyInd.get_text();
        arrIndicators[1] = this._txtInactiveInd.get_text();
        arrIndicators[2] = this._txtActiveInd.get_text();
        this._settings.set_strv(KEYS.labelIndicators, arrIndicators);
    }

    _colorCheckSet(button, settingsKey) {
        let colorString = this._settings.get_string(settingsKey);
        let color = button.get_rgba();

        if (!color.parse(colorString))
            color.parse(this._settings.get_default_value(settingsKey).unpack());

        button.set_rgba(color);
    }
});

function init() {
    ExtensionUtils.initTranslations('my-gettext-domain');
}

function buildPrefsWidget() {
    return new WorkspaceButtonsPrefsWidget();
}
